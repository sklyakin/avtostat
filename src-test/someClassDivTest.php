<?php
	//require 'src/someClass.php';

	class someClassDivTest extends PHPUnit_Framework_TestCase {
	

		/**
     	* @dataProvider provider
     	*/
		public function testDiv($a, $b) {
			$someCls = new someClass();
			$someCls->div($a, $b);
		}

		public function provider() {
			return array(
				array(1,2),
				array(3,4),
				array(5,6),
				array(3,0)
			);
		}
	}

?>