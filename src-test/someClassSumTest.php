<?php

	require 'src/someClass.php';

	class someClassTest extends PHPUnit_Framework_TestCase {

		/**
     	* @dataProvider provider
     	*/
		public function testSum($a, $b) {
			$someCls = new someClass();
			$someCls->sum($a, $b);
		}
		


		public function provider() {
			return array(
				array(1,2),
				array(3,4),
				array(5,6),
				array(3,0)
			);
		}

	}
?>